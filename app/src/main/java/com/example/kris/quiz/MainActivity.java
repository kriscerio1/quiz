package com.example.kris.quiz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static com.example.kris.quiz.Common.NAME;
import static com.example.kris.quiz.Common.EMAIL;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText name = (EditText) findViewById(R.id.text_name);
        final EditText email = (EditText) findViewById(R.id.text_email);
        Button next = (Button) findViewById(R.id.button_next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent instruction = new Intent (MainActivity.this, Instruction.class);
                instruction.putExtra(NAME , name.getText().toString());
                instruction.putExtra(EMAIL, email.getText().toString());

                if (name.getText().length()==0) {
                    name.setError("Please Enter A Name");
                }
                else if (email.getText().length()==0) {
                    email.setError("Please Enter An Email Adress");
                }
                else
                    startActivity(instruction);
            }
        });
    }
}
