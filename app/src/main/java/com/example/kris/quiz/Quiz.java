package com.example.kris.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

/**
 * Created by kris on 8/31/15.
 */

import static com.example.kris.quiz.Common.NAME;
import static com.example.kris.quiz.Common.EMAIL;
import static com.example.kris.quiz.Common.QUESTION;
import static com.example.kris.quiz.Common.ANSWER;

public class Quiz extends Activity {

    private Handler handler;
    private static final String TAG =" " ;
    private String answer="";
    Button next,previous,submit;
    TextView txtquestion;
    RadioButton True,False;
    int current,score;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        initialVariables();
        txtquestion.setText(QUESTION[current]);
    }

    public void initialVariables() {
        txtquestion = (TextView) findViewById(R.id.txtQuestion);
        True = (RadioButton) findViewById(R.id.rbtnTrue);
        False = (RadioButton) findViewById(R.id.rbtnFalse);
        handler = new Handler();
        current = 0;
        score = 0;
    }

     public void onNextClick(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        nextQuestion();
                    }
                });
            }
        }).start();
    }

    public void onPreviousClick (View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        previousQuestion();
                    }
                });
            }
        }).start();
    }

    public void onSubmitClick(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Submit();
                    }
                });
            }
        }).start();

    }


    int result=0;

    private void nextQuestion () {
        current+=1;
        if (current >= QUESTION.length) {
            current = QUESTION.length-1;
            txtquestion.setText(QUESTION[current]);
        }else {
            txtquestion.setText(QUESTION[current]);
        }
        if (True.isChecked()) {
            if (ANSWER[current].contentEquals("true")) {
                score+=1;
//                Toast.makeText(Quiz.this, "Score: "+score, Toast.LENGTH_SHORT).show();
//                if (current >= QUESTION.length) {
//                    current = QUESTION.length-1;
//                    Toast.makeText(Quiz.this, "Score: "+score, Toast.LENGTH_SHORT).show();
//                }
            }else {
                score+=0;

            }

        }
    }

    private void previousQuestion () {
        if (current == 0) {
            txtquestion.setText(QUESTION[current]);
            return;
        }

        current-=1;
        if (current >= QUESTION.length) {
            current = QUESTION.length - 1;
            txtquestion.setText(QUESTION[current]);
            return;
        }
        txtquestion.setText(QUESTION[current]);

    }

    private void Submit () {
        Intent i = new Intent(Quiz.this, ResultActivity.class);
        i.putExtra(ResultActivity.SCORE,score);
        startActivity(i);








    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "OnStart() was called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "OnResume() was called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() was called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() was called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "OnRestart() was called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() was called");
    }


}
