package com.example.kris.quiz;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by kris on 8/31/15.
 */
public class ResultActivity extends Activity{

    public static final String EXTRA_OPERATION = "extra:operation";
    public static final String SCORE = "extra:score";
    int score;
    String operation;
    TextView txtscore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        initialVariable();
        txtscore.setText(""+score);
    }

    public void initialVariable(){
        operation = getIntent().getStringExtra(EXTRA_OPERATION);
        score = getIntent().getIntExtra(SCORE, 0);
        txtscore = (TextView) findViewById(R.id.txtAnswer);
    }

}
