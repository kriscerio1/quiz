package com.example.kris.quiz;

/**
 * Created by kris on 8/31/15.
 */
public class Common {

    public static final String NAME = "intent:name";
    public static final String EMAIL = "intent:email";

    public static final String QUESTION [] = {
        "0 - Is Kris Cerio Handsome? ",
        "1 - Is he Cute? ",
        "2 - Is he Strong? ",
        "3 - Is he Kind? ",
        "4 - Is he Firm? ",

        "5 - Is he Duhhh? ",
        "6 - Is true , true? ",
        "7 - Is false, false?  ",
        "8 - 1+1=2? ",
        "90 - Is Sir Ahdz Handsome?  "

    };

    public static final String ANSWER [] = {
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",
        "true",

    };
}
