package com.example.kris.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by kris on 8/31/15.
 */

import static com.example.kris.quiz.Common.NAME;
import static com.example.kris.quiz.Common.EMAIL;

public class Instruction extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);

        Button start = (Button) findViewById(R.id.button_start);
        String name = getIntent().getStringExtra(NAME);
        String email = getIntent().getStringExtra(EMAIL);
        Toast.makeText(this, "name=" + name + ", email=" + email, Toast.LENGTH_LONG).show();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent quiz = new Intent(Instruction.this,Quiz.class);
                startActivity(quiz);
            }
        });
    }
}
